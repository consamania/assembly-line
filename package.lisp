
(defpackage #:assembly-line
  (:use :cl)
  (:export #:assembly-line
           #:$value

           #:*end-of-assembly-line*
           #:line-ended-p
           #:line-value-p
           #:with-value
           #:get-value
           #:get-values
           #:call-with-values
           #:with-assembly-line

           #:identity-transform
           #:modify
           #:filter
           #:buffer-n
           #:skip-n
           #:skip-until
           #:skip-first
           #:stop-if
           #:check-first
           #:expand
           #:subsample
           #:down-sample
           #:check-sorted
           #:group-n
           #:group
           #:uniq
           #:head-n
           #:stratified-head-n
           #:drop-errors #:drop-value
           #:endable #:stop

           #:make-source
           #:make-sink
           #:list-iterator
           #:nil-ended-iterator

           #:sorted-equi-join))
