
(in-package :assembly-line)

(defun sorted-equi-join (iter1 iter2 get-key1 get-key2 comparator compare-equal
                         &key
                           (check-sortedness t)
                           (join-type :inner))
  "Do a join of two assembly lines. Assuming the inputs ITER1 and
ITER2 are sorted by their respective keys (to be extracted using
GET-KEY1 and GET-KEY2 respectively), use COMPARATOR and COMPARE-EQUAL
to return an inner/equi-join on their values. If you are sure that the
inputs from ITER1 and ITER2 will definitely be sorted, the sort order
checks can be turned off by setting :CHECK-SORTEDNESS to NIL."
  (let ((giter1 (group iter1 get-key1))
        (giter2 (group iter2 get-key2)))
    (when check-sortedness
      (let ((comparator* (lambda (x y)
                           (or (funcall comparator x y)
                               (funcall compare-equal x y)))))
        (setf giter1 (check-sorted giter1 'first comparator*))
        (setf giter2 (check-sorted giter2 'first comparator*))))
    (let* ((sense t)
           (current-group (funcall giter1))
           (buffer (list)))
      (lambda ()
        (block main
          (if buffer
              (pop buffer)
              (flet ((cross (xs ys)
                       (setf buffer
                             (loop
                                for x in xs
                                append (loop
                                          for y in ys
                                          collect (list x y)))))
                     (finish ()
                       (return-from main *end-of-assembly-line*)))
                (macrolet ((do-join-type ((join-type) inner-body left-body)
                             `(ecase ,join-type
                                (:inner ,inner-body)
                                (:left ,left-body))))
                  (loop
                     (loop
                        for match-group = (funcall giter2)
                        while (and (line-value-p current-group)
                                   (line-value-p match-group)
                                   (funcall comparator (first match-group) (first current-group)))
                        do (do-join-type (join-type)
                             nil
                             (unless sense
                               (cross (second match-group) (list nil))
                               (return-from main (pop buffer))))
                        finally (progn
                                  ;; (format t "~a ~a~%" current-group match-group)
                                  (cond ((and (line-ended-p current-group)
                                              (line-ended-p match-group))
                                         (finish))
                                        ((line-ended-p match-group)
                                         (do-join-type (join-type)
                                           (finish)
                                           (if sense
                                               (progn
                                                 (cross (second current-group) (list nil))
                                                 (setf current-group (funcall giter1))
                                                 (return-from main (pop buffer)))
                                               (finish))))
                                        ((line-ended-p current-group)
                                         (do-join-type (join-type)
                                           (finish)
                                           (if sense
                                               (finish)
                                               (progn
                                                 (cross (second match-group) (list nil))
                                                 (return-from main (pop buffer))))))
                                        ((funcall compare-equal (first match-group) (first current-group))
                                         (if sense
                                             (cross (second current-group) (second match-group))
                                             (cross (second match-group) (second current-group)))
                                         (setf current-group (funcall giter1))
                                         (return-from main (pop buffer)))
                                        (t (let ((initial-sense sense))
                                             (do-join-type (join-type)
                                               nil
                                               (when initial-sense
                                                 (cross (second current-group) (list nil))))
                                             (psetf current-group match-group
                                                    giter1 giter2
                                                    giter2 giter1
                                                    sense (not sense))
                                             (do-join-type (join-type)
                                               nil
                                               (when initial-sense
                                                 (return-from main (pop buffer))))))))))))))))))
