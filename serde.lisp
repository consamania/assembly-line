
(in-package :assembly-line)

(defgeneric make-source (input type)
  (:documentation "Make an assembly-line (SOURCE of values) that reads
  from INPUT. Common TYPEs are :csv and :cl-store, in which case INPUT
  is a stream that will be read using CSV-PARSER:READ-CSV-LINE and
  CL-STORE:RESTORE respectively."))

(defgeneric make-sink (output type)
  (:documentation "Make a SINK that can accept values and process them
  appropriately, usually sending them in turn to output. eg if TYPE
  is :csv then OUTPUT is a STREAM written to using
  CSV-PARSER:WRITE-CSV-LINE."))

(defgeneric flood-fill (assembly-line sink &key max-count)
  (:documentation "Retrieve every value from ASSEMBLY-LINE and send
  them to SINK, one by one."))

;; ====================

(defmethod make-source (stream (type (eql :csv)))
  (lambda ()
    (or (csv-parser:read-csv-line stream)
        *end-of-assembly-line*)))

(defmethod make-sink (stream (type (eql :csv)))
  (lambda (value)
    (csv-parser:write-csv-line stream value)))

(defmethod make-source (stream (type (eql :cl-store)))
  (let ((done nil))
    (lambda ()
      (if done
          *end-of-assembly-line*
          (handler-case
              (cl-store:restore stream)
            (cl:end-of-file (e)
              (declare (ignore e))
              (setf done t)
              *end-of-assembly-line*))))))

(defmethod make-sink (stream (type (eql :cl-store)))
  (lambda (value)
    (cl-store:store value stream)))

(defmethod make-source (list (type (eql :list)))
  (lambda ()
    (if list
        (pop list)
        *end-of-assembly-line*)))

;; ====================

;; Convenience tool, usually used for testing/debugging.
(defun list-iterator (list)
  (make-source list :list))

(defun nil-ended-iterator (iterator)
  "Convert our iterator to an iterator that returns NIL at the end,
instead of the internal *END-OF-ASSEMBLY-LINE* placeholder. This is
sometimes what many Lisp programmers expect or be more used to."
  (lambda ()
    (with-value (v (funcall iterator))
      v
      nil)))
