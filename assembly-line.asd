
(defsystem #:assembly-line
  :version "0.0.0"
  :licence "CC0"
  :author "consamania"
  :long-description "A library for setting up assembly lines made from sequential processing stages."
  :depends-on ("alexandria"
               "csv-parser"
               "cl-store")
  :serial t
  :components
  ((:file "package")
   (:file "assembly-line")
   (:file "serde")
   (:file "sorted-join")))

(defsystem #:assembly-line/test
  :version "0.0.0"
  :licence "CC0"
  :author "consamania"
  :long-description "Tests for ASSEMBLY-LINE."
  :depends-on ("assembly-line"
               "fiveam")
  :serial t
  :components
  ((:file "tests")))
