
(defpackage #:assembly-line/test
  (:use :cl :assembly-line :fiveam)
  (:export #:assembly-line
           #:retrieving-value
           #:retrieving-values
           #:stages-1
           #:stages-2
           #:many-stages
           #:sort))

(in-package :assembly-line/test)

(def-suite assembly-line
    :description "Tests for ASSEMBLY-LINE.")

(in-suite assembly-line)

(defmacro is-equal (expr1 expr2)
  `(is (equal ,expr1 ,expr2)))

(test retrieving-value
  (is-equal 1 (get-value (list-iterator '(1 2 3))))
  (is-equal nil (get-value (list-iterator '())))
  (is-equal :ended (get-value (list-iterator '()) :default-value :ended)))

(test retrieving-values
  (is-equal (get-values (list-iterator '(1 2 3 4 5 5 6 7 3 8)))
            '(1 2 3 4 5 5 6 7 3 8))

  (is-equal (get-values (list-iterator '(1 2 3 4 5 5 6 7 3 8))
                        :max-count 5)
            '(1 2 3 4 5))

  (is-equal (let ((line (list-iterator '(1 2 3 4 5 5 6 7 3 8))))
              (list (get-values line :max-count 3)
                    (get-values line :max-count 3)))
            '((1 2 3) (4 5 5))))

(defmacro simple-test ((&optional (inputs '(list-iterator '(1 2 3 4 5 5 6 7 3 8))))
                       &body body)
  `(get-values
    (assembly-line ,inputs
        ,@body)))

(test stages-1
  (is-equal (get-values
             (assembly-line (list-iterator '(1 2 3 4))
               ;; confirm that an empty body = identity-transform
               ))
            '(1 2 3 4))

  (is-equal (simple-test ()
              (identity-transform))
            '(1 2 3 4 5 5 6 7 3 8))

  (is-equal (simple-test ()
              (modify (+ $value 1)))
            '(2 3 4 5 6 6 7 8 4 9))

  (is-equal (simple-test ()
              (expand (list $value (+ $value 10))))
            '(1 11 2 12 3 13 4 14 5 15 5 15 6 16 7 17 3 13 8 18))

  (is-equal (simple-test ((list-iterator '(1 2 3 4 5 6 7)))
              (check-sorted $value '<))
            '(1 2 3 4 5 6 7))

  (signals error
    (simple-test ((list-iterator '(1 2 3 4 5 5 6 7 3 8)))
      (check-sorted $value '<=)))

  (signals error
    (simple-test ((list-iterator '(1 2 3 4 5 6 5 4)))
      (check-sorted $value '<)))

  (is-equal (handler-bind ((error
                            (lambda (e)
                              (declare (ignore e))
                              (invoke-restart 'continue))))
              (simple-test ((list-iterator '(1 2 3 4 5 6 5 4)))
                (check-sorted $value '<)))
            '(1 2 3 4 5 6))

  (is-equal (simple-test ()
              (buffer-n 3))
            '(1 2 3 4 5 5 6 7 3 8))

  ;; Actually test the buffering sequence.
  (is-equal (let ((events (list)))
              (let ((line (let ((values '(1 2 3 4 5 6 7 8 9 10)))
                            (lambda ()
                              (let ((value (pop values)))
                                (push (list :pop value) events)
                                (or value *end-of-assembly-line*))))))
                (loop
                   with buffered-line = (buffer-n line (constantly 3))
                   for value = (get-value buffered-line)
                   while value
                   do (push (list :get-value value) events)))
              (reverse events))
            '((:POP 1)
              (:POP 2)
              (:POP 3)
              (:GET-VALUE 1)
              (:GET-VALUE 2)
              (:GET-VALUE 3)
              (:POP 4)
              (:POP 5)
              (:POP 6)
              (:GET-VALUE 4)
              (:GET-VALUE 5)
              (:GET-VALUE 6)
              (:POP 7)
              (:POP 8)
              (:POP 9)
              (:GET-VALUE 7)
              (:GET-VALUE 8)
              (:GET-VALUE 9)
              (:POP 10)
              (:POP NIL)
              (:GET-VALUE 10)
              (:POP NIL)))

  (is-equal (simple-test ()
              (skip-n 3))
            '(4 5 5 6 7 3 8))
  (is-equal (simple-test ()
              (skip-until (> $value 5)))
            '(6 7 3 8))

  (is-equal (let ((first-value nil))
              (list (simple-test ()
                      (skip-first (push $value first-value)))
                    first-value))
            '((2 3 4 5 5 6 7 3 8) (1)))

  (is-equal (simple-test ((list-iterator '(("a" "b" "c")
                                           (1 2 3)
                                           (4 5 6))))
              (check-first '("a" "b" "c")))
            '((1 2 3) (4 5 6)))

  (signals error
    (simple-test ((list-iterator '(("a" "b" "c")
                                   (1 2 3)
                                   (4 5 6))))
      (check-first '("x" "y" "z")))))

(test stages-2
  (is-equal (simple-test ((list-iterator '(0 1 2 3 4 5 6 7 8 9 10 11 12 13 14)))
              (down-sample 3))
            '(0 3 6 9 12))

  (is-equal (simple-test ()
              (head-n 3))
            '(1 2 3))

  (is-equal (simple-test ((list-iterator '((1 0) (1 1) (1 2) (1 3) (1 4) (1 5)
                                           (2 0) (2 1) (2 2) (2 3) (2 4) (2 5)
                                           (3 0) (3 1) (3 2) (3 3) (3 4) (3 5)
                                           (4 0) (4 1) (4 2) (4 3) (4 4) (4 5)
                                           (5 0) (5 1) (5 2) (5 3) (5 4) (5 5))))
              (stratified-head-n (elt $value 0)
                                 '((1 . 2)
                                   (2 . 6)
                                   (3 . 4)
                                   (4 . 0)
                                   (5 . 1))))
            '((1 0) (1 1)
              (2 0) (2 1) (2 2) (2 3) (2 4) (2 5)
              (3 0) (3 1) (3 2) (3 3)
              (5 0)))

  (signals error
    ;; not enough elements at strata 1 (6 required, only 3 present).
    (simple-test ((list-iterator '((1 0) (1 1) (1 2)
                                   (2 0) (2 1) (2 2) (2 3) (2 4) (2 5))))
      (stratified-head-n (elt $value 0) '((1 . 6) (2 . 4) (3 . 1)))))

  (is-equal (simple-test ((list-iterator '((1 0) (1 1) (1 2)
                                           (2 0) (2 1))))
              (stratified-head-n (elt $value 0) '((1 . 1) (2 . 0))))
            '((1 0)))

  (is-equal (simple-test ()
              (filter (evenp $value)))
            '(2 4 6 8))

  (is-equal (simple-test ()
              (stop-if (> $value 5)))
            '(1 2 3 4 5 5))

  (is-equal (simple-test ()
              (group-n 3))
            '((1 2 3) (4 5 5) (6 7 3) (8)))

  (is-equal (simple-test ((list-iterator (list 1 2 3)))
              (group-n 3))
            '((1 2 3)))

  (is-equal (simple-test ((list-iterator (list)))
              (group-n 3))
            ())

  (is-equal (simple-test ()
              (group (floor $value 3)))
            '((0 (1 2)) (1 (3 4 5 5)) (2 (6 7)) (1 (3)) (2 (8))))

  (is-equal (simple-test ((list-iterator (list)))
              (group 1))
            '())

  (is-equal (simple-test ((list-iterator '(1 1 1 2 3 4 4 5 5 6 7 8 8)))
              (uniq $value))
            '(1 2 3 4 5 6 7 8))

  (is-equal (simple-test ((list-iterator '((1 a) (1 b) (1 c)
                                           (2 d)
                                           (3 e)
                                           (4 f) (4 g)
                                           (5 h) (5 i)
                                           (6 j)
                                           (7 k)
                                           (8 l) (8 m))))
              (uniq (elt $value 0)))
            '((1 A) (2 D) (3 E) (4 F) (5 H) (6 J) (7 K) (8 L)))

  (is-equal (handler-bind ((error (lambda (e)
                                    (declare (ignore e))
                                    (invoke-restart 'drop-value))))
              (simple-test ()
                (modify (progn
                          (when (= $value 5)
                            (error "Test."))
                          $value))
                (drop-errors "Drop this value.")))
            '(1 2 3 4 6 7 3 8))

  (is-equal (handler-bind ((error (lambda (e)
                                    (declare (ignore e))
                                    (invoke-restart 'stop))))
              (simple-test ()
                (modify (progn
                          (when (= $value 5)
                            (error "Test."))
                          $value))
                (endable "Stop processing values.")))
            '(1 2 3 4)))

#+nil
(simple-test ((list-iterator '(0 1 2 3 4 5 6 7 8 9 10 11 12 13 14)))
  (subsample 3 (make-random-state t)))

(test many-stages
  (is-equal (let ((source (list-iterator '(("a" 1 2)
                                           ("b" 2 3)
                                           ("c" 3 4)
                                           ("d" 4 5)
                                           ("e" 5 6)))))
              (get-values
               (assembly-line source
                 (drop-errors "Skip reading value.")
                 (modify (list (elt $value 1) (elt $value 2)))
                 (identity-transform)
                 (expand (let ((sum (+ (elt $value 0) (elt $value 1))))
                           (list (list sum (elt $value 1) (elt $value 0))
                                 (list sum (* (elt $value 1) 10) (elt $value 0))
                                 (list sum (* (elt $value 1) 100) (elt $value 0)))))
                 (modify (list (elt $value 0) (elt $value 1) (elt $value 2) "10"))
                 (filter (evenp (elt $value 1)))
                 (endable "Stop here."))))
            '((3 2 1 "10")
              (3 20 1 "10")
              (3 200 1 "10")
              (5 30 2 "10")
              (5 300 2 "10")
              (7 4 3 "10")
              (7 40 3 "10")
              (7 400 3 "10")
              (9 50 4 "10")
              (9 500 4 "10")
              (11 6 5 "10")
              (11 60 5 "10")
              (11 600 5 "10"))))

(test sort
  (is-equal
   (get-values
    (sorted-equi-join (list-iterator '((1 a) (2 a1) (2 a2) (3 a) (4 a1) (4 a2) (5 a) (6 a) (8 a) (10 a)))
                      (list-iterator '((0 b) (1 b1) (1 b2) (2 b) (3 b) (4 b) (5 b) (6 b) (7 b) (8 b) (9 b)))
                      'first
                      'first
                      '< '=))
   '(((1 A) (1 B1))
     ((1 A) (1 B2))
     ((2 A1) (2 B))
     ((2 A2) (2 B))
     ((3 A) (3 B))
     ((4 A1) (4 B))
     ((4 A2) (4 B))
     ((5 A) (5 B))
     ((6 A) (6 B))
     ((8 A) (8 B))))

  (is-equal
   (get-values (sorted-equi-join (list-iterator '(1 2 2 3 4 5 6 7 8))
                                 (list-iterator '((1 a) (2 a) (3 a) (4 a) (4 b) (4 c) (5 a) (5 b) (6 a) (8 a) (9 a)))
                                 'identity
                                 'car
                                 '< 'equal))
   '((1 (1 A))
     (2 (2 A))
     (2 (2 A))
     (3 (3 A))
     (4 (4 A))
     (4 (4 B))
     (4 (4 C))
     (5 (5 A))
     (5 (5 B))
     (6 (6 A))
     (8 (8 A))))

  (is-equal
   (get-values
    (sorted-equi-join (list-iterator '((1 a) (2 a1) (2 a2) (3 a) (4 a1) (4 a2) (5 a) (6 a) (8 a) (10 a)))
                      (list-iterator '((0 b) (1 b1) (1 b2) (2 b) (3 b) (4 b) (5 b) (6 b) (7 b) (8 b) (9 b)))
                      'first
                      'first
                      '< '=
                      :join-type :left))
   '(((1 A) (1 B1))
     ((1 A) (1 B2))
     ((2 A1) (2 B))
     ((2 A2) (2 B))
     ((3 A) (3 B))
     ((4 A1) (4 B))
     ((4 A2) (4 B))
     ((5 A) (5 B))
     ((6 A) (6 B))
     ((8 A) (8 B))
     ((10 A) NIL)))

  (is-equal
   (get-values
    (sorted-equi-join (list-iterator '((1 a) (2 a1) (2 a2) (3 a) (4 a1) (4 a2) (5 a) (6 a) (8 a) (9 a) (10 a)))
                      (list-iterator '((0 b) (1 b1) (1 b2) (3 b) (4 b1) (4 b2) (6 b) (7 b) (8 b) (11 b)))
                      'first
                      'first
                      '< '=
                      :join-type :left))
   '(((1 A) (1 B1))
     ((1 A) (1 B2))
     ((2 A1) NIL)
     ((2 A2) NIL)
     ((3 A) (3 B))
     ((4 A1) (4 B1))
     ((4 A1) (4 B2))
     ((4 A2) (4 B1))
     ((4 A2) (4 B2))
     ((5 A) NIL)
     ((6 A) (6 B))
     ((8 A) (8 B))
     ((9 A) NIL)
     ((10 A) NIL)))

  (is-equal
   (get-values
    (sorted-equi-join (list-iterator '((1 a) (2 a1) (2 a2) (3 a) (4 a1) (4 a2) (5 a) (6 a) (8 a) (10 a)))
                      (list-iterator '((0 b) (1 b1) (1 b2) (3 b) (4 b) (6 b) (7 b) (8 b) (9 b)))
                      'first
                      'first
                      '< '=
                      :join-type :left))
   '(((1 A) (1 B1))
     ((1 A) (1 B2))
     ((2 A1) NIL)
     ((2 A2) NIL)
     ((3 A) (3 B))
     ((4 A1) (4 B))
     ((4 A2) (4 B))
     ((5 A) NIL)
     ((6 A) (6 B))
     ((8 A) (8 B))
     ((10 A) NIL))))

