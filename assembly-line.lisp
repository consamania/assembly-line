
(in-package :assembly-line)

(defun tidy-specs (processors)
  (or processors
      (list '(identity-transform))))

(defmacro assembly-line (line &body processors)
  "Create an ordered sequence of data processors declaratively, return an assembly-line."
  (loop
     for (processor body . others) in (tidy-specs processors)
     for prev-var = line then var-name
     for var-name = (gensym (symbol-name processor))
     collect (list var-name `(,processor
                              ,prev-var
                              (lambda ($value)
                                (declare (ignorable $value))
                                ,body)
                              ,@others))
     into stages
     finally (return
               `(let* (,@stages) ,var-name))))

;;; ====================

(defparameter *end-of-assembly-line*
  (make-symbol "END-OF-ASSEMBLY-LINE"))

(defun line-ended-p (value)
  (eq value *end-of-assembly-line*))

(defun line-value-p (value)
  (not (eq value *end-of-assembly-line*)))

(defmacro with-value ((var value) then &optional (else '*end-of-assembly-line*))
  "Bind VAR to the result of the VALUE expression, and depending on
whether there are more values or if we are at the end of an assembly
line, perform the THEN or ELSE actions. This looks like a IF-LET
construct."
  `(let ((,var ,value))
     (if (eq ,var *end-of-assembly-line*)
         ,else
         ,then)))

(defun get-value (assembly-line &key (default-value nil))
  "Retrieve one value from the ASSEMBLY-LINE."
  (with-value (value (funcall assembly-line))
    value
    default-value))

(defun get-values (assembly-line &key (max-count nil))
  "Retrieve multiple values from the ASSEMBLY-LINE, up till
MAX-COUNT (if provided) or till the ASSEMBLY-LINE ends."
  (loop
     for count upfrom 0
     for continue-p = (if max-count
                          (< count max-count)
                          t)
     for value = (if continue-p
                     (funcall assembly-line)
                     *end-of-assembly-line*)
     while (and continue-p (line-value-p value))
     collect value))

(defun call-with-values (line fun
                         &key
                           (max-count nil))
  "Call FUN repeatedly with values retrieved from LINE, up till
MAX-COUNT (if provided) times, or till the end of the LINE."
  (loop
     for count upfrom 0
     for continue-p = (if max-count
                          (< count max-count)
                          t)
     for value = (if continue-p
                     (funcall line)
                     *end-of-assembly-line*)
     while (and continue-p (line-value-p value))
     do (funcall fun value)))

(defmacro with-assembly-line ((line &key max-count) &body body)
  "Perform some actions with values from the assembly LINE."
  `(call-with-values ,line
                     (lambda ($value)
                       (declare (ignorable $value))
                       ,@body)
                     :max-count ,max-count))

;;; ====================

;;; Some default stages or data processors, these form the various
;;; processors on our data assembly line. Each assembly line stage, or
;;; data processor, takes a line (an iterator represented by a
;;; function) and returns a new (longer by one stage) line by
;;; appending its operations at the end of the input line. All stages
;;; have argument list:
;;;
;;; (input-assembly-line function &rest [or &optional] additional-args)
;;;
;;; with FUNCTION acting as a priviledged argument. It takes 1
;;; argument (typically a value retrieved from the previous stage of
;;; the INPUT-ASSEMBLY-LINE), and returns an argument to be used in
;;; combination with ADDITIONAL-ARGS (if any). This explanation is
;;; still not very clear... see some examples or tests.

(defun identity-transform (line &rest args)
  "No actions - the output from this stage is the input to this stage."
  (declare (ignore args))
  line)

(defun modify (line modifier)
  "Change or transform the input value into a different output value
using the MODIFIER."
  (lambda ()
    (with-value (value (funcall line))
      (funcall modifier value))))

(defun filter (line filter)
  "Pass only values (unchanged) that satisfy the FILTER predicate,
drop the rest."
  (lambda ()
    (loop
       (with-value (value (funcall line))
         (when (funcall filter value)
           (return value))
         (return value)))))

(defun buffer-n (line get-n)
  "An internal store for up to N elements, to provide a way to batch
retrieval operation for subsequent stages of the assembly line. There
are no externally visible changes otherwise."
  (let ((buffer (list))
        (n (funcall get-n nil)))
    (flet ((refill-buffer ()
             (setf buffer
                   (loop
                      for i below n
                      for value = (funcall line)
                      while (line-value-p value)
                      collect value))))
      (lambda ()
        (if buffer
            (pop buffer)
            (progn
              (refill-buffer)
              (if buffer
                  (pop buffer)
                  *end-of-assembly-line*)))))))

(defun skip-n (line get-n)
  "Skip (drop) the first N values before resuming normal iteration, to
pass on the remaining values unchanged."
  (let ((n (funcall get-n nil))
        (skipped nil))
    (lambda ()
      (if skipped
          (funcall line)
          (progn
            (loop
               repeat n
               for value = (funcall line)
               until (line-ended-p value))
            (setf skipped t)
            (funcall line))))))

(defun skip-until (line test)
  "Skip (drop) all values until a value that satisfies test if found,
pass on that value and all remaining values (ignore/stop TESTing the
values) from this point onwards, unchanged."
  (let ((found-p nil))
    (lambda ()
      (if found-p
          (funcall line)
          (loop
             (with-value (value (funcall line))
               (when (funcall test value)
                 (setf found-p t)
                 (return value))
               (return value)))))))

(defun skip-first (line fun)
  "Drop the first value (but pass it to FUN), and pass on the
remaining values (unmodified)."
  (let ((first-time-p t))
    (lambda ()
      (let ((value (funcall line)))
        (if first-time-p
            (with-value (value value)
              (progn
                (funcall fun value)
                (setf first-time-p nil)
                (funcall line)))
            value)))))

(defun stop-if (line tester)
  "Pass values through untouched, unless TESTER returns
true (non-nil/generalized boolean), in which case, stop retrieving any
more values and stop passing any more values (return
*END-OF-ASSEMBLY-LINE* on subsequent invocations)."
  (let ((ended-p nil))
    (lambda ()
      (if ended-p
          *end-of-assembly-line*
          (with-value (value (funcall line))
            (cond ((funcall tester value)
                   (setf ended-p t)
                   *end-of-assembly-line*)
                  (t value)))))))

(defun check-first (line get-expected-value &key (test 'equal))
  "Test and check that the first value from assembly line is
CL:EQUAL (default) to some expected value. Only the first value is
tested and dropped. All subsequent values are passed through
untouched."
  (let ((expected-value (funcall get-expected-value nil)))
    (skip-first line (lambda (found-value)
                       (assert (funcall test found-value expected-value)
                               (found-value expected-value)
                               "Mismatched 'first' value, found ~a, expected ~a."
                               found-value expected-value)))))

(defun expand (line expander)
  "One value goes into this stage, multiple values come out,
duplicating each input value into several output values, except the
outputs can also be transformed (by EXPANDER) along the way. The
EXPANDER may return the empty list (LIST) or NIL, which means the
value is dropped (no outputs), processing continues with the next
value to be expanded."
  (let ((expansion (list)))
    (lambda ()
      (if expansion
          (pop expansion)
          (loop
             (with-value (value (funcall line))
               (progn
                 (setf expansion (funcall expander value))
                 (when expansion
                   (return (pop expansion))))
               (return value)))))))

(defun subsample (line get-n &optional seed)
  "Randomly (using the provided seed) sample (pass on, unmodified) one
out of every N values, and drop the rest."
  (let ((n (funcall get-n nil)))
    (when seed
      (setf *random-state* seed))
    (lambda ()
      (loop
         (with-value (value (funcall line))
           (when (zerop (random n))
             (return value))
           (return value))))))

(defun down-sample (line get-n)
  "Sample one (pass on unmodified) out of every N values regularly.
Start counting from, and keep, the first value, then drop all the rest
till the next N-th value."
  (let ((n (funcall get-n nil)))
    (lambda ()
      (prog1
          (funcall line)
        (loop
           repeat (- n 1)
           for value = (funcall line)
           until (line-ended-p value))))))

(defun check-sorted (line get-key comparator*)
  "Check that the elements in LINE is sorted according to COMPARATOR*.
The * in the comparator is there to highlight that it should also
return T when elements are equal. This is in contrast to CL:SORT's
comparator (called a 'predicate') which has to return NIL when the
elements are equal."
  (let* ((uninitialised '#:uninitialised)
         (previous-key uninitialised))
    (lambda ()
      (loop
         (with-value (current (funcall line))
           (let ((current-key (funcall get-key current)))
             (when (or (eq previous-key uninitialised)
                       (funcall comparator* previous-key current-key))
               (setf previous-key current-key)
               (return current))
             (cerror "Ignore this value, and continue."
                     "Found an out-of-order value: ~S~% vs previous-key: ~S"
                     current previous-key))
           (return current))))))

(defun group-n (line get-n)
  "Collect the values from LINE into groups of N items. ie each output
value is a list of (value1 value2 value3 ...), each list being of
length N (except for the last list, which may contain fewer than N
items if there isn't enough values in the assembly LINE)."
  (let ((done nil)
        (n (funcall get-n nil)))
    (lambda ()
      (if done
          *end-of-assembly-line*
          (loop
             repeat n
             for value = (funcall line)
             if (line-value-p value)
             collect value into group
             else
             do (setf done t)
             and return (or group
                            *end-of-assembly-line*)
             end
             finally (return (or group
                                 *end-of-assembly-line*)))))))

(defun group (line get-key)
  "Collect multiple input values together into a group: each group
tests for EQUALity on key = (funcall GET-KEY value), and is passed on
as (KEY GROUP), where group is a list of (value1 value2 value3...).
That is, (MAPCAR GET-KEY GROUP) will return a list of EQUAL KEYs.

All values belonging to the same group must be passed through the
assembly LINE together, often, this means you'll want the values in
the assembly LINE to be sorted beforehand.

Note that the GET-KEY should be side-effect free - it may be invoked
more than once per value."
  (let* ((store (list))
         (uninitialised '#:uninitialised))
    (flet ((get-next ()
             (if (null store)
                 (funcall line)
                 (pop store)))
           (restore (value)
             (push value store)))
      (lambda ()
        (loop
           with current-key = uninitialised
           with group = (list)
           for value = (get-next)
           if (line-ended-p value)
           return (if (eq current-key uninitialised)
                      *end-of-assembly-line*
                      (list current-key (reverse group)))
           else
           do (let ((key (funcall get-key value)))
                (when (eq uninitialised current-key)
                  (setf current-key key))
                (if (equal key current-key)
                    (push value group)
                    (progn
                      (restore value)
                      (return (list current-key (reverse group))))))
           end)))))

(defun uniq (line &optional (get-key 'identity) &key (test 'equal))
  "Similar to Unix's uniq, if consecutive values TESTed the same with
key retrieved using GET-KEY, then drop the duplicates, and return only
the first value. Usually, the input values need to be pre-sorted to
ensure duplicate values are 'next' to each other."
  (let* ((uninitialised '#:uninitialised)
         (previous-key uninitialised))
    (lambda ()
      (loop
         (with-value (current (funcall line))
           (let ((current-key (funcall get-key current)))
             (when (or (eq previous-key uninitialised)
                       (not (funcall test current-key previous-key)))
               (setf previous-key current-key)
               (return current)))
           (return current))))))

(defun head-n (line get-n)
  "Pass through only the first N (obtained from funcalling GET-N)
values unchanged, stop early if we find ourselves at the end of the
assembly LINE. Drop all subsequent values. If N is NIL, pass through
all values."
  (let ((n (funcall get-n nil))
        (count 0))
    (if n
        (lambda ()
          (setf count (min (1+ count) (1+ n)))
          (if (<= count n)
              (funcall line)
              *end-of-assembly-line*))
        line)))

(defun stratified-head-n (iterator get-stratum strata-max-counts-alist
                          &key
                            (test 'equal)
                            (check-max t))
  "Given an ordered stream of values coming off the assembly-line
ITERATOR, take the first few (as specified) values for each
group (called a stratum here) of values.

For example, we could specify that we will only pass on the top 10
values for :high and :medium values, and top 20 :low values.

(assembly-line (list-iterator '((:high 0) (:high 1) ... (:medium 0)
                                ... (:low 0) (:low 1) ...)
  ...
  (stratified-head-n (el 0) '((:high . 10) (:medium . 10) (:low . 20)))
  ...)

CHECK-MAX will ensure each stratum (note: except the last one!) has
the maximum number of elements. The last stratum cannot be checked
within an assembly line because we have no way of knowing when we are
at the end. We don't (yet?) have actions that we can perform after
accessing the last element. (eg signal/indicate on-last-element,
TODO?)"
  (let* ((uninitialised '#:uninitialised)
         (current-stratum uninitialised)
         (stratum-count 0)
         (max-count nil))
    (flet ((get-max-count (stratum)
             ;; wish we can (easily) rename the error and its
             ;; description into something more descriptive at this
             ;; level.
             (cdr (or (assoc stratum strata-max-counts-alist :test test)
                      (error "Could not find stratum: ~a" stratum)))))
      (lambda ()
        (loop
           (with-value (value (funcall iterator))
             (let ((stratum (and value (funcall get-stratum value))))
               (cond ((eq uninitialised current-stratum)
                      (setf current-stratum stratum)
                      (setf max-count (get-max-count stratum))
                      (setf stratum-count 1))
                     (t
                      (incf stratum-count)))
               (if (funcall test stratum current-stratum)
                   (if (<= stratum-count max-count)
                       (return value)
                       nil)
                   (progn
                     (when check-max
                       (assert (> stratum-count max-count)
                               (stratum-count max-count)
                               "Not enough elements in the current stratum: ~d @ ~a, expecting at least ~d."
                               stratum-count current-stratum max-count))
                     (setf current-stratum stratum)
                     (setf stratum-count 1)
                     (setf max-count (get-max-count stratum))
                     (if (> max-count 0)
                         (return value)
                         nil))))
             (return value)))))))

(defun drop-errors (line get-message)
  "Provide the DROP-VALUE restart to allow us to optionally discard
any values that cause errors while being processed in our assembly
LINE.

When an error is encountered while processing a $value, this stage
provides the option to discard that value and continue
processing (passing on) subsequent values."
  (let ((message (funcall get-message nil)))
    (lambda ()
      (block done
        (loop
           (restart-case
               (return-from done (funcall line))
             (drop-value (&optional exception)
               :report (lambda (stream)
                         (write-string message stream))
               (declare (ignore exception)))))))))

(defun endable (line get-message)
  "Provides the STOP restart to allow us to end all work upon
encountering any errors. The STOP restart will not retrieve or process
any more values from the LINE."
  (let ((message (funcall get-message nil))
        (continue-p t))
    (lambda ()
      (restart-case (if continue-p
                        (funcall line)
                        *end-of-assembly-line*)
        (stop (&optional exception)
          :report (lambda (stream) (write-string message stream))
          (declare (ignore exception))
          (setf continue-p nil)
          *end-of-assembly-line*)))))

